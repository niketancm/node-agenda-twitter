const express = require("express");

const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const twitter = require('twitter');
const Agenda = require("agenda");

const config = require('./configs/config');
const db = require('./configs/db');
const { MongoClient } = require("mongodb");

const app = express();

const mongo = "mongodb://localhost:27017/twitter";
// const conn = db.connectToMongo();
db.connectToMongo();

app.use(express.json());
app.use(bodyParser.json());
app.use(cookieParser());

var twitClient = new twitter(config);
// twitClient.get('search/tweets', {q:'blockchain,bitcoin'}, function(error, tweets, response){
//     let a = tweets["statuses"]
//         let input = []
//         for(let i = 0; i < a.length; i++){
//             let temp = {
//                 "created_at": a[i].created_at,
//                 "id": a[i].id,
//                 "text": a[i].text,
//                 "username": a[i].user.name,
//                 "user_description": a[i].user.description,
//                 "retweet_count": a[i].retweet_count
//             }
//             input.push(temp)
//             let collec = db.get().collection('tweetData');
//             collec.updateOne({"id": a[i].id},{$set:temp},{upsert:true});
//         }
// })
(async function run(){
    const agenda = new Agenda({
        db:{address: mongo, collection:'data'}
    })
    
    agenda.define('get and store',(job, done) =>{
        twitClient.get('search/tweets', {q:'blockchain,bitcoin'}, function(error, tweets, response){
            let a = tweets["statuses"];
            // console.log(tweets);
            let a = tweets["statuses"]
            let input = []
            for(let i = 0; i < a.length; i++){
                let temp = {
                    "created_at": a[i].created_at,
                    "id": a[i].id,
                    "text": a[i].text,
                    "username": a[i].user.name,
                    "user_description": a[i].user.description,
                    "retweet_count": a[i].retweet_count
                }
                input.push(temp)
                let collec = db.get().collection('tweetData');
                collec.updateOne({"id": a[i].id},{$set:temp},{upsert:true});
            }
        }, done())
    })
    await agenda.start();
    await agenda.every('30 minutes', 'get and store')
})();


app.get("/getCount", (req, res, next) => {
    db.get().collection("tweetData").aggregate([
        {
            $group:{
                '_id':"$username",
                count:{$sum:1}
                },
            }
    ], function(err, cursor){
        cursor.toArray(function(err, docs){
            let result = [];
            for(let i =0; i < docs.length; i++){
                let temp = {};
                temp["name"] = docs[i]["_id"];
                temp["count"] = docs[i]["count"];
                result.push(temp);
            }
            res.send(result);
        });
    });
});

app.get("/sortDesending",(req, res, next) =>{
    db.get().collection("tweetData").find().sort({"retweet_count": -1}).toArray()
    .then(docs=>{
        let sortArr = docs.sort((a,b)=>{
            return b.retweet_count - a.retweet_count;
        })
        // console.log(sortArr);
        let ans = [];
        for(let i = 0; i < sortArr.length; i++){
            let temp = {};
            temp["username"] = sortArr[i]["username"];
            temp["retweet_count"] = sortArr[i]["retweet_count"];
            ans.push(temp);
        }
        // console.log(ans);
        res.send(ans);
        dbClient.close()
    })
});

app.listen(8080, () => {
  console.log("App now listening to 8080 port");
});