const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017';

const dbName = 'twitter';

let dbClient = new MongoClient(url, {
    useNewUrlParser: true,
    // maxTimeMS: 1800000,
    keepAlive: 1800000,
    connectTimeoutMS: 1800000,
    socketTimeoutMS: 1800000
});

dbClient.connect(function(err, client){
    let db = client.db(dbName);
    let user ="Blockchain Knight";
    // db.collection('tweetData').aggregate([
    //     {
    //         $group:{
    //             '_id':"$username",
    //             count:{$sum:1}
    //             },
    //         }
    // ]).toArray()
    // .then(docs =>{
    //     let res = [];
    //     for(let i =0; i < docs.length; i++){
    //         let temp = {};
    //         temp["name"] = docs[i]["_id"];
    //         temp["count"] = docs[i]["count"];
    //         res.push(temp)
    //     }
    //     console.log(res);
    //     dbClient.close();
    // })
    db.collection("tweetData").find().sort({"retweet_count":-1}).toArray()
        .then(docs => {
            // console.log(docs);
            //sort just in case
            let sortArr = docs.sort((a,b)=>{
                return b.retweet_count - a.retweet_count;
            })
            // console.log(sortArr);
            let ans = [];
            for(let i = 0; i < sortArr.length; i++){
                let temp = {};
                temp["username"] = sortArr[i]["username"];
                temp["retweet_count"] = sortArr[i]["retweet_count"];
                ans.push(temp);
            }
            console.log(ans);
            dbClient.close()
        })
})