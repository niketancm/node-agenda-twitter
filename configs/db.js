const MongoClient = require('mongodb').MongoClient;

let _db;
const url = 'mongodb://localhost:27017';
const dbName = 'twitter';

const options = {
    useNewUrlParser: true,
    reconnectTries: 5000,
    poolSize: 25
}

async function connectToMongo(){
    try{

        let client = await MongoClient.connect(url, options);
        _db = client.db(dbName);
        return _db;

    }catch(err){

        console.log(`$$$$$$$$$$$$$$$`);
        console.log(`Error connecting to the db`);
        console.log(err);
        console.log(`$$$$$$$$$$$$$$$`);

    }
}

function get(){
    return _db;
}

module.exports = {connectToMongo, get}